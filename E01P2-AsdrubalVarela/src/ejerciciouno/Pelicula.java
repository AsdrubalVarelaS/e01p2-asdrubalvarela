/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciouno;

/**
 *
 * @author asdru
 */
public class Pelicula {

    private String categoria;
    private String nombre;
    private int min;

    public Pelicula() {
    }

    public Pelicula(String categoria, String nombre, int min) {
        this.categoria = categoria;
        this.nombre = nombre;
        this.min = min;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setMin(int min) {
        this.min = min;
    }
    public int getMin() {
        return min;
    }

    @Override
    public String toString() {
        return "La película " + nombre + "\npertenece a la categoría"
                + categoria + "\ny su duración es de " + min;
    }

}
